# [spinor-helicity-identities](https://gitlab.com/eidoom/spinor-helicity-identities/)

Identities and their proofs for use with the spinor helicity formalism. 

[Live here](https://eidoom.gitlab.io/spinor-helicity-identities/helicity-nitty-gritty.pdf)
